

[toc]

Here's an image of the contents of this doc since Bitbucket doesn't seem to be supporting clickable links in the table of contents. 

![Table of contents](images/0_add_an_image_of_toc.png)

# Lesson 1: Documentation with Git

## Getting some things out of the way

* I assume you have already downloaded Git. If not, please download and install from [here](https://git-scm.com/). This should also download Git Bash. I assume you are working on a Windows machine. 
* I assume you have basic familiarity with a statistical package like Stata. Though the examples in this walkthrough will reference Stata, feel free to substitute any other statistical tool that you want: SPSS, SAS, R, Python. Even image files/simple text files will work. At the heart of it all, Git tracks changes and versions, so regardless of what file you choose, keep making changes as I make changes in this walkthrough. They don't even have to be the exact same changes as long as they as in the same spirit.
* I will not attempt to get into too much detail about Git as there are excellent books[^1] that do this well. This is meant to be a simple, practical walkthrough to get you to see the utility of Git. If you end up using Git to manage your files for even one small project, then I have achieved my goal. A lot of the examples/analogies in this walkthrough will be very simplified, and while they may not represent the nuances of what's happening in Git, they will help you develop an intuitive and practical understanding.

Launch Git Bash, which I will also refer to as the shell. When it opens up, notice the `$` at the start of a line. This means the shell is waiting for user input. Type this in (without the dollar sign).

```shell
$ git version
```

If you get output that mentions a git version, congratulations, you are ready to dive into the rest of this lesson. For example, here's what my output for `git version` looks like.

```shell
git version 2.33.0.windows.2
```

For the rest of this document, whenever a sample of code starts with a `$` sign, know that it is referring to a line of code that you need to input into the shell (excluding the dollar sign). _**To paste text into the shell, use Shift + Insert or right click + paste as opposed to Control + V**_.

# Navigating to a directory

In your shell, type this in, `pwd` stands for _"Print working directory"_.

```shell
$ pwd 
```

The output may look like this.

```shell
/h/
```

This is telling you that the shell is currently living in the **H:** folder in your computer. This is not what we want because all of our Git commands will act on the **H:** folder as a whole. Instead, say you want Git to work on a specific folder you created on your desktop called _my_git_lesson_1_. You will have to tell Git that. In other words, you will have to change directories using the `cd` command. Right click on your desired folder and figure out its path, and enter that path into the shell using forward slashes both at the beginning and also to separate sub-directories. Here's what this looks like for me (note: the path preceding the _my_git_lesson_1_ folder may be different in your computer.)

```shell
$ cd /c/users/lbalaji/my_git_lesson_1
```

Now if you enter the `pwd` command again, you will see this directory echoed. That means the  working directory has changed.

```
c/users/lbalaji/my_git_lesson_1
```

To make things more complicated, what if you'd named your folder _lesson1 documentation_ and created it as a subfolder of a larger folder called _Git For Clinical Research_, which was itself living in the **H:** folder? How would you change directories? Nope, `$ cd /h/Git For Clinical Research/lesson1 documentation` will not work.  You will get a response that says `bash: cd: too many arguments`. You need to **escape** the spaces in the folder names, i.e, you need to tell the shell that a space is coming up by using a backslash right before the space. Try this.

```shell
$ cd /h/Git\ For\ Clinical\ Research/lesson1\ documentation
```

> To be able to follow along, just ensure you have a 'lesson1 documentation' subfolder within a '_Git For Clinical Research_' folder, and substitute the `/h/` in my code for whatever is present on your machine.

That should have successfully changed the working directory to the _lesson1 documentation_ subfolder within the _Git For Clinical Research_ folder. Notice we used backslashes to escape the spaces (each space needs its own backslash immediately preceding it) and used forward slashes as usual to show subfolders within folders. If you found this annoying, this should hopefully convince you to avoid using spaces while naming folders. Use snake_case instead!

All right, on to the next step.

# Initializing a Git Repository

Imagine you've been asked to help a medical doctor analyze some lung data that he has collected. He shoots you a Stata dataset over email, called _lung.dta_ [^2], and you go ahead and save it in the _lesson1 documentation_ subfolder within the _Git For Clinical Research_ folder on your workstation.

This is what that looks like—

![This is what that looks like —](/images/1_add_a_file_without_git_init.png "Your sparse directory")

> Download the lung.dta dataset from [here](https://stats.idre.ucla.edu/other/examples/pma5/), the third bullet point says Stata and has this file along with 3 other datasets.

## Changing the file without Git

Within five minutes of receiving the email, you get a phone call from the doctor who tells you that there were two errors in the dataset. He tells you that even before you begin the analysis, he would like you to delete the last two rows of the dataset as those patients were not enrolled in the study. He also tells you to change the age of the father (_fage_) in the 5th record from 46 to 48.  These are minor changes. Assume at this stage that you implement these directly in the dataset without a do-file (you fire up the data editor window in Stata to do this) and overwrite the dataset. 

This is what your directory looks like now.

![This is what your directory looks like now.](/images/2_add_a_file_without_git_init_alter.png)

Notice that all that has been changed is the timestamp under the 'Date modified' section. The _lung.dta_ has fundamentally changed, but exactly how it was changed/why it has been changed was not recorded anywhere, it just exists in your head as instructions from a phone call.  This may be fine for now, but say for some reason, six months down the line, you are reorganizing/moving your files and you inadvertently delete the dataset. No big deal, right? You still have the original email that the doctor sent you. You only have to redownload the _lung.dta_ file and jump back into the analysis. However, since so much time has elapsed since you first started working on this project,  you may have forgotten about the tiny little phone call you had with the doctor that told you to make those two changes right at the start. So essentially, if you now begin your analysis, your input/upstream _lung.dta_ dataset is different from what the doctor intended it to be. Not good.

Here's where we begin getting into why Git is useful.

## Making the folder a Git Repository

We're starting over. Delete the changed _lung.dta_ file. Redownload the original _lung.dta_ dataset from the doctor (without the changes) and place it in the _lesson1 documentation_ folder. 

Open up the shell and make sure you're in the right directory using `pwd`. Now, enter this line.

```shell
$ git init
```

You should get output that looks like this.

```shell
Initialized empty Git repository in H:/Git For Clinical Research/lesson1 documentation/.git/
```

What does that mean for you? Go to your file explorer, hit the 'View' tab on top and check the empty box next to 'Hidden Items'. This should display hidden folders in your file explorer.  Check this out.

![Check this out.](/images/3_show_the_git_directory.png)

There's a hidden folder called _.git_ that has popped up. This is good news! It means our `git init` command worked, and Git created a folder to prepare for all the tasks that you may throw at it in the future. The word `init` is short for 'initialize'. This folder is what helps Git work, it will persist throughout the life of your project and perhaps even beyond;  **it is important**. To make sure you don't delete it by mistake, go back to the 'View' tab and hide the _.git_ folder. The fact that your _lesson1 documentation_ folder has a _.git_ folder associated with it makes it a Git repository.

> You will usually need to run the `git init` command only once per folder. In this case, your repository is the **lesson1 documentation** folder. Ensure that the folder in which you are creating a repository has atleast one file in it. From now on, it doesn't matter if you create subfolders inside this folder, Git is ready to work on them all.

In the shell, type in:

```shell
$ git status
```

You should see this as output.

```shell
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        lung.dta

nothing added to commit but untracked files present (use "git add" to track)
```

Notice the message that says 'Untracked files:' and see that it mentions _lung.dta_. This is basically Git telling you, "Hey, I'm just letting you know that I am up and running in this folder. I notice you have a dataset in this folder called _lung.dta_, I have no idea where it came from, do you want me to start tracking it?"

And the answer, as we will see in the next section, is "Of course!" 



# Staging and Commits

## Your first stage

So Git knows that you have a file called `lung.dta` sitting in your working directory. You can use the `git add` command to formally introduce the file to Git. Try this:

```shell
$ git add lung.dta
```

> **Note: when making any reference to files in the shell, you must specify the correct extension for the file.** For example, if this was a PowerPoint file that you wanted to stage, you would write `$ git add slides.pptx`. If it was an image file, you would stage it by typing in `$ git add image1.png`.

This process is called _staging_. As the name suggests, this is a way to put Git's spotlight on your file of interest. Git now keeps track of the file. You won't see any output when you stage a file. However, when you run the `$ git status` line, you see this output:

```shell
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   lung.dta

```

Notice how this message is different compared to the first output you got from  `$ git status`. You no longer see the phrase 'Untracked files'. Instead, you see a line that says there is a new file called _lung.dta_. 

Why do we stage files? The hint to that is in the output: it says changes need to be committed. The short answer is, staging files is akin to lining up a bunch of boisterous schoolchildren before taking a group photograph. Committing is the simultaneous act of actually taking that photograph, and scribbling a mandatory message at the back of the photograph. Here, you have exactly one rowdy schoolchild: the _lung.dta_ file. 

> Unlike the `git init` command which only needs to be run once in a repository, the `git add file.xx` command needs to be run once per change per file in your repository that you want to track.

Let's take a photograph.

## Your first ever commit

Thanks to your staging, Git's eyes are peeled and watching this `lung.dta` file closely, but it really doesn't know anything about the file — Git doesn't know what the file is about, who sent it to you, or when they sent it to you and so on. Let's use the commit command to add this info as a note.

Try this.

```shell 
$ git commit -m "This is where it all begins...the lung dataset that the doctor sent me."
```

> Note: no need to mention file names in a git commit command. Any files that have been staged using the add command will automatically be considered part of the commit.  All you need to worry about is the commit message: you cannot make a commit without adding a message. The -m flag tells Git that a message is about to follow, and the within the quotes, you enter the body of the message.

You will see output that looks like this:

```shell
[master (root-commit) 5cc0413] This is where it all begins...the lung dataset that the doctor sent me.
 Committer: Balaji <lbalaji@bidmc.harvard.edu>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly. Run the
following command and follow the instructions in your editor to edit
your configuration file:

    git config --global --edit

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author

 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 lung.dta

```

Ignore the information about the automatic configuration — Git is telling you how it recorded the author of the commit and tells you how to change author information. What's more important is the first part of the commit, the part that says `[master (root-commit) 5cc0413] This is where it all begins..the lung dataset that the doctor sent me.`

What's happening is that Git has taken a snapshot (commit) of the file you lined up (stage) and has scribbled the _"This is where...sent me"_ message as a note on the back of the photograph. This commit has a unique ID associated with it, called the **SHA-1 ID**, and it starts it with the digits **_5cc0413_**. Congratulations, you're done with your first commit. The fact that the doctor sent you this file is now recorded forever in Git history! To verify, enter this.

```shell
$ git log --graph
```

You will see output that looks like this:

```shell
* commit 5cc041359c33f22984976f754a9dbba831cf7884 (HEAD -> master)
  Author: Balaji <lbalaji@bidmc.harvard.edu>
  Date:   Fri Oct 29 16:00:45 2021 -0400

      This is where it all begins...the lung dataset that the doctor sent me.

```

> the `--graph` flag just marks each commit with an asterisk, and connects commits with vertical lines. At this stage in your Git learning journey, consider that flag optional.
>
> Note that if you are following along to this project from scratch (i.e, without cloning my repo), you will absolutely have different commit SHA-1 IDs. That shouldn't matter for the purposes of this walkthrough, as long as you are able to figure out what commit I am referring to.

That is the entire history of your project thus far. This message is telling you that on Friday, October 29th, at around 4 PM a person called Balaji, with the email address `lbalaji@bidmc.harvard.edu` made a commit with that specific commit ID, and shows the message that the person entered.  The message says that a certain "lung" dataset, sent by a doctor, is saved in this repository.

> Note: you do not need to remember the full commit ID. The way SHA-1 commit IDs are designed, even the first few components, for example, the characters 5cc041 are enough to help Git retrieve a commit that you want. 

Now, if you enter `$ git status`, you will see this:

```shell
On branch master
nothing to commit, working tree clean
```

This means everything is fine, and Git is waiting for you to proceed.

## Your second commit

Now, using the data editor in Stata, make the two changes that the doctor wanted you make (delete the last two rows, change the _fage_ value in the 5th record from 46 to 48). Overwrite and save the dataset. It should have 148 rows in total now.

This is what your directory looks like now.

![current state of directory](/images/4_add_a_file_with_git_init.png)

It doesn't look too different from the [earlier stab](#changing-the-file-without-git) we took at this change, right? Wrong.  Remember, this folder is now a Git repository, and the file _lung.dta_ is being tracked by Git. Run $ `$ git status` and see what happens.

```shell
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   lung.dta

no changes added to commit (use "git add" and/or "git commit -a")

```

This is cool! Git has correctly identified that the file _lung.dta_ was modified. Let's line up the file for the photograph.

```shell
$ git add lung.dta
```

And now, let's photograph the file and scribble a message at the back of the photograph.

```shell 
$ git commit -m "Per phone call with the doc, deleted the last two rows and changed fage on row 5"
```

Now, view the log.

```shell 
$ git log --graph
```

```shell
* commit 5d68514b2e3b1cc83148846a5ec9cef040338d5d (HEAD -> master)
| Author: Balaji <lbalaji@bidmc.harvard.edu>
| Date:   Fri Oct 29 17:05:38 2021 -0400
|
|     Per phone call with the doc, deleted the last two rows and changed fage on row 5
|
* commit 5cc041359c33f22984976f754a9dbba831cf7884
  Author: Balaji <lbalaji@bidmc.harvard.edu>
  Date:   Fri Oct 29 16:00:45 2021 -0400

      This is where it all begins...the lung dataset that the doctor sent me.

```

Notice there are two commits now. The first commit (**_5cc041_**) is a snapshot of your folder right after the original dataset was saved, and the second commit (**_5d6851_**) is a snapshot of your folder right after the dataset was modified to remove the last two rows and change the _fage_ variable for row 5.

So there you have it, you made a change and incorporated that change into the history of your project. So now, no matter when you visit this folder, if you fire up the shell and type in `$ git log`, you will be able to see what happened since you started. 

>  If staging is like lining up people for a photograph, and committing is like actually taking the photograph, then the act of viewing the log is like flipping through an old-school photo album, where each photo is an individual commit, and the back of each photo has a commit message that tells you about the context of the photo. The album is the project in its entirety.

Here's a cartoon I drew (I take every chance I can to draw) explaining how `git status`, `git add` and `git commit` work together.

![cartoon](/images/16_cartoon_add_commit.png)



# Practice with commits and staging

Now that you know the basics of staging and commits, let's try out a couple of examples to mirror what would happen in a real world analysis, and in the process, we will explore some additional functionality of commits and staging. We will continue working with the _lung.dta_ file.

Let's create a do-file called _lungscript.do_ to begin analyzing _lung.dta_, and populate it with some simple analysis code.

![This is the start of the analysis](/images/5_start_a_simple_analysis.png)

The analysis code that _lungscript.do_ holds is intended to compare the mean of the _fev1_ between fathers and mothers.

```SAS
use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* let's do a t-test
ttest ffev1 == mfev1, unpaired
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers
```

## Entering a lengthier commit message 

We're happy with this analysis, so we stage and commit the do-file with an appropriate message.

```shell
$ git add lungscript.do
$ git commit
```

Up to now, we were using the commit command with the -m flag. Sometimes, if your message is long, it may get wrapped and will be difficult to read. But, when we enter the command `$ git commit` without a `-m` flag (as we did above), the shell will say `hint: waiting for your editor to close the file` and the display will shift to show something like this. 

![Lengthier commit message screen 1](/images/6_lengthier_commit_1.png)

You should have an phrase that says `-- INSERT` at the bottom. If you don't see it, just press the letter 'i' on your keyboard and you should see it pop up. Once that happens, you can begin typing your commit message. Here, the message you type is clearer and easier to read.

Here's how I entered my message.

![6_lengthier_commit_2](/images/6_lengthier_commit_2.png)

I used a title in uppercase (shows up in yellow), then had an empty line, and then had a couple of descriptive lines in the body (show up in grey) detailing what happens in this commit, (i.e, a do-file was created and some code was added to compare `fev1` by gender of parent). Best practices are to wrap the title at 50 characters and the body at 72 characters so that they don't run off the page when you call `git log`. Here are some [more guidelines](https://chris.beams.io/posts/git-commit/#why-not-how) on writing good commit messages from Chris Beam.

Once you're done entering the message, hit `Esc` and then type in the words `:wq`. This should complete the commit and shift you back to your shell display.

> Note: as long as you enter a commit message, it doesn't matter too much whether you use the -m flag to enter shorter messages or whether you use the window to enter lengthier messages.  Use the window if you want to exert finer control over spaces and line breaks in your commit message.

Now run `git log`. You will see the history of your project up to this point.

> Tip 1: If you have trouble switching out of git log, try typing the word 'q' and hitting enter. 
>
> Tip 2: If you want a shorter version of the log, use the --oneline flag (will print just the commit titles and not the descriptions)

```shell 

$ git log --oneline
b73ddc2 (HEAD -> master) FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
5cc0413 This is where it all begins...the lung dataset that the doctor sent me.

```

Commits are displayed in a reverse chronological fashion. The most recent commit is the one on the top (with the **SHA-1** of **_b73ddc2_**), and it refers to the point where we completed the t-test.

## Continuing the commit chain

All of a sudden,  you notice that the variance of the _fev1_ is not the same for men and women. Your independent samples t-test needs to be modified to specify the unequal variance.

So you go ahead and edit the _lungscript.do_ to allow for unequal variance and overwrite it. The content of the changed do-file is displayed below.

```SAS

use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* however, the variances of the fev1s are not the same for fathers and mothers.
* variance for men is 4189.2
su ffev1, det
* variance for women is 2402.3
su mfev1, det

* let's do a t-test, but this time, REMEMBER TO USE THE UNEQUAL OPTION
ttest ffev1 == mfev1, unpaired unequal
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers


```

Once overwritten, run `$ git status` and confirm that Git notices that _lungscript.do_ has been modified.  Now, stage and commit it.

```shell
$ git add lungscript.do
$ git commit -m "SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VAR"
```

Look at your log now.

```shell
$ git log --oneline

88bdf0e (HEAD -> master) SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VAR
b73ddc2 FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
5cc0413 This is where it all begins...the lung dataset that the doctor sent me.

```

Again, notice in your history that everything has been recorded so far- the fact that the doc sent us a dataset, and that we subsequently changed the dataset because of a request made over the phone, the fact that you did a first pass of the analysis with the default t-test settings, and then the fact that you replaced the analysis with a t-test with the "unequal variances" option specified. Also, as you get more comfortable with viewing the log, I hope you are noticing that the most recent commit (**__88bdfoe__** in this case) always has a `(HEAD  -> master)` reference attached to it. This is not something we will touch upon in this document but it will play an important role in future write-ups. For now, just remember that `HEAD` is always supposed to be _**attached to the most recent commit**_ and that there is only one `HEAD` per Git repository.

### Amending commits

Okay, now, say that we are not happy with the commit message for the most recent commit. It says "UNEQUAL VAR", but we would like it to say "UNEQUAL VARIANCE" for clarity. Or, you could just as easily imagine that there was a typo in the commit message. Git allows you to _**amend**_ the most recent commit quite easily.

```shell
$ git commit --amend -m "SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VARIANCE" 
```

Now run `$ git log --oneline`:

```shell

1cfd91c (HEAD -> master) SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VARIANCE
b73ddc2 FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
5cc0413 This is where it all begins...the lung dataset that the doctor sent me.

```

Notice that **the most recent commit** has had the commit message corrected. Also, notice that even though we didn't change anything else in the commit, the **SHA-1** has changed entirely (the original **SHA-1** of the most recent commit was **_88bdf0e_**, but now, it is *__1cfd91c__*.  

> Git usually only allows you to amend the most recent commit. If you want to change older commits, it gets more complicated and is outside the scope of this write-up. The lesson here: think hard about your commits and type them in carefully to avoid having to amend them! If you're not too sure what your commit is going to do, you could test out your commits using the `--dry-run` tag, like so: `git commit -m "testing out this commit" --dry-run`

### Renaming files

Now, let's say you want to rename the _lungscript.do_ to _lungcode.do_, it is not a good idea to do this directly in your file explorer because Git will think that you deleted the file _lungscript.do_ and that you created a brand new file called _lungcode.do_ (This means when you look at this file on a remote, you will have lost the meticulously documented iterations of this do-file: it will instead seem like it popped out of thin air with the unequal variance analysis). Instead, use the `$ git mv` command and follow it up with a `$ git commit`

```shell 
$ git mv lungscript.do lungcode.do
```

You won't see any output, but in your file explorer, you will see that the name of the do-file has changed.

![7_renaming_file](/images/7_renaming_file.png)

Now, you just need to commit the rename.

```shell 
$ git commit -m "Renamed analysis file (changed name lungscript.do to lungcode.do)"
```

Here's what the log looks like now.

```shell
$ git log --oneline

ca61f66 (HEAD -> master) Renamed analysis file (changed name lungscript.do to lungcode.do)
1cfd91c SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VARIANCE
b73ddc2 FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
5cc0413 This is where it all begins...the lung dataset that the doctor sent me.

```

### Moving files

You can use the exact same `git mv` command to move files around.

#### Background for moving files

Now, let's say that within the _lesson1 documentation_ folder, you created a folder called _Misc_, and within that, you had a text file called _coauthors.txt_ for the list of potential coauthors, and it is currently populated with just one name.

![8_movingfiles_a](/images/8_movingfiles_a.png)

```shell
$ git status

On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Misc/

```

You have already told Git to start tracking the _coauthors.txt_ file using `git add`.  We  originally had just one coauthor.

```shell 
$ git add Misc/ 
$ git commit -m "Started Misc subfolder: current list of coauthors - we just have Lakshman"

```

> Since coauthors.txt is located within Misc, you need to specify the subfolder Misc/ while referring to the file. If you say `git add Misc/`, everything within the Misc subfolder is staged. If you want to be more precise, you would say `git add Misc/couathors.txt`

Now, imagine that over the next few weeks, after a lot of meetings, the coauthor list was meticulously populated with 3 extra names, and the rationale for their additions. Here's what the commits look like.

```shell
$ git log --oneline

550aed4 (HEAD -> master) Adding Mike for contribution to manuscript writing
14fe4c2 Adding Ari; helped a lot with securing database access and coordinating with Parth
471a508 Adding Anne as coauthor as she helped with analysis plan
191c6d0 Started Misc subfolder: current list of coauthors - we just have Lakshman
ca61f66 Renamed analysis file (changed name lungscript.do to lungcode.do)
1cfd91c SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VARIANCE
b73ddc2 FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
5cc0413 This is where it all begins...the lung dataset that the doctor sent me.
```

#### Actually moving the file

Notice that the 4 most recent commits pertain to the _coauthor.txt_ file within the _Misc_ folder within the _lesson1 documentation_ folder. Now say we want to move the _coauthor.txt_ file out of the _Misc_ folder and into the _lesson1 documentation_ folder. If we directly moved from the directory, and later viewed this file on a remote, we may lose this iterative history of how we added coauthors. Instead, it is better to use the `git mv` command.

```shell 
$ git mv Misc/coauthors.txt coauthors.txt
```

Notice that the first file reference says `Misc/coauthors.txt` while the second reference says `coauthors.txt`. It just means that you want the _coauthors.txt_ file to be moved out of the _Misc_ subfolder.  Let's commit the move.

```shell 
$ git commit -m "Moved coauthors.txt out to directory from the Misc subfolder and deleted the subfolder"
```

![8_movingfiles_b](/images/8_movingfiles_b.png)

This is what the directory looks like at this stage. Notice that if you tried this command:

```shell 
$ git log --follow coauthors.txt
```

You will get the full history for the file despite the fact that it was moved, this is because `git mv` helped Git identify that the moved file was not a new file but an older file with a location change.

Notice that the _Misc_ subfolder is still present. Let's delete it manually using the file explorer.  There is no need to record this in Git because there is nothing in the folder. Your folder now looks like this and the move is complete.

![8_movingfiles_c](/images/8_movingfiles_c.png)

### Staging intricacies

This is unrelated to the _lung.dta_ example, but it is important to know. Say you're working on a results write-up where you modified not just the write-up (a readme file), but you also added 7 images.

```shell
$ git status

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        images/5_start_a_simple_analysis.png
        images/6_lengthier_commit_1.png
        images/6_lengthier_commit_2.png
        images/7_renaming_file.png
        images/8_movingfiles_a.png
        images/8_movingfiles_b.png
        images/8_movingfiles_c.png

```

Here, the `README.md` file has been changed and has not been staged. Also, 7 image files have been newly created and not been staged yet. You can do several things here. For example:

1. You can stage the `README.md` first by typing in `$ git add README.md` and then just do a `$ git commit -m "Adding readme"` now, and it will commit the write-up. You could then `$ git add` the individual image files or the folder in which the images are present and then do another commit `$ git commit -m "Adding 7 images"`. _**But this will create two separate commits in your log, what if you wanted to commit this all in one go?**_
2. You can *unstage an already added file* by using the `$ git reset` command. For example, if the write-up was already staged, you can unstage it by saying `$ git reset README.md`. Now, you can add all the files in one staging session, by doing something like `$ git add README.md images/5_start_a_simple_analysis.png.... images/8_movingfiles_c.png` (list all the file names individually here) and then commit them all, using `$ git commit -m "Adding the readme and 7 images`. _**This will create just one commit on the log.**_
3. If you delete a file, you should still stage it. Say you delete your `README.md`. It no longer shows up in your directory, but you should still let Git know that it's gone by saying `git add README.md` and then commit the deletion by saying `git commit -m "Deleted the readme"`

# Tagging important checkpoints



Up to now, we have been referring to individual commits using their _**SHA-1**_ IDs, which are quite difficult to remember. Also, some commits may be more important than others (for example, a commit that holds the files that were submitted to a journal is naturally more important that a commit where a file was just renamed). An easier way to refer to commits would be to name them, or **_tag them_**.  Here, we walk through an example where we put a manuscript together and tag the commit when the manuscript was submitted.

Back to the _lung.dta_ example. As a recap, we've now completed a t-test analysis (unequal variance) on the dataset, and added a list of coauthors to our repo. Now, say we do a simple correlation analysis between the height of the father (_fheight_)/height of the mother (_mheight_) and the height of the oldest child (_ocheight_) and also add scatterplots. This is what our do-file looks like at this stage.

```SAS
*******************************
* Do FILE TO ANALYZE LUNG.DTA *
*******************************

use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* however, the variances of the fev1s are not the same for fathers and mothers.
* variance for men is 4189.2
su ffev1, det
* variance for women is 2402.3
su mfev1, det

* let's do a t-test, but this time, REMEMBER TO USE THE UNEQUAL OPTION
ttest ffev1 == mfev1, unpaired unequal
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers

* Is there a correlation between the father's height/mother's height and the 
* oldest child's height?
pwcorr fheight mheight ocheight, sig
* Correlation for father - oldest child height (0.16)
* Correlation for mother - oldest child height (0.10)

* create and save scatterplots
scatter fheight ocheight, title("Father heights and oldest child heights") subtitle("Correlation is 0.16")
graph export scatter1.png
scatter mheight ocheight, title("Mother heights and oldest child heights") subtitle("Correlation is 0.10")
graph export scatter2.png


```

We also use this opportunity to create a _results.docx_ for submission to a manuscript. 

![9_manuscriptv1](/images/9_manuscriptv1.png)

This is what the repository looks like at this stage.

![10_repo_at_manuscriptv1](/images/10_repo_at_manuscriptv1.png)

As you can see visually, the _lungcode.do_ was modified online and three new files have been added: a _results.docx_ manuscript, a _scatter1.png_ and _scatter2.png_. Use `git status` to confirm that these have been noticed by Git.

```shell 
$ git status

On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   lungcode.do

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        results.docx
        scatter1.png
        scatter2.png

```

Now, it is up to you to decide how to stage and commit these. You can do them all in one go, or you can add them incrementally to get the best out of version control. I choose to stage the do-file and the saved images together, and the manuscript after that (Creating a total of 2 commits).

```shell
$ git add lungcode.do scatter1.png scatter2.png
$ git commit -m "THIRD PASS AT ANALYSIS: ADDED CORRELATION BETWEEN PARENT HEIGHTS AND OLDEST CHILD HEIGHT, ADDED CODE TO SAVE SCATTERPLOTS, AND SAVED 2 SCATTERPLOT IMAGES AS PNGS"


$ git add results.docx
$ git commit -m "First manuscript version, prepared for submission to NEJM"
```

Here's your project history so far.

```shell
$ git log --graph --oneline

* 453aa8a (HEAD -> master) First manuscript version, prepared for submission to NEJM
* ec91313 THIRD PASS AT ANALYSIS: ADDED CORRELATION BETWEEN PARENT HEIGHTS AND OLDEST CHILD HEIGHT, ADDED CODE TO SAVE SCATTERPLOTS, AND SAVED 2 SCATTERPLOT IMAGES AS PNGS
* c43c5ca Moved coauthors.txt out to directory from the Misc subfolder and deleted the subfolder
* 550aed4 Adding Mike for contribution to manuscript writing
* 14fe4c2 Adding Ari; helped a lot with securing database access and coordinating with Parth
* 471a508 Adding Anne as coauthor as she helped with analysis plan
* 191c6d0 Current list of coauthors - we just have Lakshman
* ca61f66 Renamed analysis file (changed name lungscript.do to lungcode.do)
* 1cfd91c SECOND PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER, UNEQUAL VARIANCE
* b73ddc2 FIRST PASS AT ANALYSIS: TTEST OF FEV1 BY GENDER
* 5d68514 Per phone call with the doc, deleted the last two rows and changed fage on row 5
* 5cc0413 This is where it all begins...the lung dataset that the doctor sent me.

```

The most recent commit, (_**SHA-1**_ of _**453aa8a**_) is the commit that holds the first version of the manuscript that we have ambitiously submitted to the New England Journal of Medicine. We can highlight/tag/name this commit using the `git tag` command.

```shell 
$ git tag firstNEJM -m "First version of manuscript submitted to NEJM" 453aa8
```

* All tag commands start with the phrase `git tag`
* Notice the words 'firstNEJM'. This is the name of the tag.  Notice the **_SHA-1_** at the end. We are saying that we want commit _**453aa8a**_ to be tagged with the 'firstNEJM' name, and so in the future, we can pull up this commit directly without using the **_SHA-1_**. 
* Notice the `-m` flag. This allows us to add a descriptive message: `First version of manuscript submitted to NEJM` to this commit.

Now, when you run the `git log` command with the `--tags` flag, you will see this commit highlighted in yellow.

```shell
$ git log --oneline --graph --tags
```

![11_tag_in_yellow](/images/11_tag_in_yellow.png)

Tagging basically makes it easier to see the important "checkpoints" in your project. Use the `$ git tag` to see all tags associated with a project. Once you identify a tag of interest, you call up the commit message from these checkpoints to jog your memory as to what actually happened at that checkpoint.

```shell 
$ git tag
$ git show firstNEJM
```

> Note: deleting a tag is easy. In this case, you can type in `$ git tag -d firstNEJM` and Git will delete the tag for you. Specify the name of the tag (`NEJM` in this instance) after the `-d` flag.

Say we hear back from the reviewers at NEJM, and they want us to get rid of the correlation analysis and both associated images, and put in a linear regression analysis, with the outcome variable as the height of the oldest child (_ocheight_), and the predictor variables of age of the oldest child (_ocage), father height (_fheight_) and mother height (_mheight_). We can edit the do-file to reflect that.

```SAS
*******************************
* Do FILE TO ANALYZE LUNG.DTA *
*******************************

use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* however, the variances of the fev1s are not the same for fathers and mothers.
* variance for men is 4189.2
su ffev1, det
* variance for women is 2402.3
su mfev1, det

* let's do a t-test, but this time, REMEMBER TO USE THE UNEQUAL OPTION
ttest ffev1 == mfev1, unpaired unequal
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers

* Per reviewer request,
* Removed correlation analysis, 
* and now we 
* regress ocheight on ocage, fheight, mheight.
regress ocheight ocage fheight mheight
* Coefficient for ocage is 1.98
* Fheight is 0.36
* Mheight is 0.28
```

We manually delete the two images (_scatter1.png_ and _scatter2.png_).

We then revise the word document to reflect the changed results.

![12_manuscriptv2](/images/12_manuscriptv2.png)

This is what our repo looks like at this point.

![13_repo_at_manuscriptv2](/images/13_repo_at_manuscriptv2.png)

As usual, we stage and commit the changes.

```shell
$ git add lungcode.do scatter1.png scatter2.png # notice we stage the images even though they were deleted.
$ git commit -m "FOURTH PASS AT ANALYSIS: ADDED REGRESSION OF OLDEST CHILD HEIGHT ON OLDEST CHILD AGE CONTROLLING FOR PARENT HEIGHTS. REMOVED CORRELATION BETWEEN PARENT HEIGHTS AND OLDEST CHILD HEIGHT, REMOVED CODE TO SAVE SCATTERPLOTS, AND DELETED 2 SCATTERPLOT PNGS"

$ git add results.docx
$ git commit -m "Second manuscript version, just has unequal variances t-test and regression model"

```

And we will tag this recent manuscript commit.

```shell
$ git tag secondNEJM -m "This commit tagged to mark status of directory at the time of resubmitting to NEJM after first round of revisions" 1b38767
```

Now, let's view the log.

```shell
$ git log --oneline --graph --tags
```

![14_tag_in_yellow_again](/images/14_tag_in_yellow_again.png)

Notice that we can clearly see the corrections made to the results file since the **firstNEJM** tag in the commit _**4c2fbad**_, and we can see the second tag _**secondNEJM**_ clearly indicating the second important submission to NEJM.

I hope this section was helpful in demonstrating the utility of tagging important commits. We will now move on to the last section to talk about how you can use Git as a time machine to view your project at important points.

# Git Checkout Time Traveling

So far we have done a lot in this project. To summarize:

1. Received a dataset from our doctor collaborator.
2. Edited the dataset per telephone instructions  from the doc.
3. Created a do-file for analysis and ran a default t-test
4. Modified the t-test to allow for unequal variances.
5. Changed name of the do-file.
6. Populated a list of coauthors incrementally, initially within a Misc subfolder but later deleted the subfolder.
7. Changed analysis file to include a correlation analysis.
8. Prepared a manuscript with unequal variances t-test + correlation analysis + 2 scatterplot images.
9. Changed analysis file to remove the correlation analysis and include a regression analysis instead. Deleted the 2 scatterplots.
10. Prepared a manuscript with unequal variance t-test + regression analysis.

Our directory has been in constant flux throughout this process, with files being created, destroyed, renamed, moved and so on. At the end of step 10, this is what our directory looked like.

![13_repo_at_manuscriptv2](/images/13_repo_at_manuscriptv2.png)

_**What I consider to be the coolest feature of Git is that you can use the `git checkout` command to temporarily view what your folder (lesson1 documentation) looked like at other points in its history!**_

Remember, the manuscript has already been resubmitted. But say you wanted to glimpse what the manuscript looked like at the point of the first submission.  You would type this:

```shell
$ git checkout firstNEJM
```

Here's an abridged version of the output from the shell.

```shell
Note: switching to 'firstNEJM'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

HEAD is now at 453aa8a First manuscript version, prepared for submission to NEJM
```

* First off, notice that you referenced the commit that represented the manuscript at the point where it was first submitted using a tag (which we already created in the previous section). If you don't have a tag for your commit of interest, you need to specify the _**SHA-1**_. For example, had we not tagged this commit, we would use the log to find out that this commit's _**SHA-1**_ is _**453aa8a**_, and would complete the checkout command like so: `$ git checkout 453aa8a`.

* Secondly, notice the warning about being in the `detached HEAD` state. Remember when I mentioned earlier that the `HEAD` always points to the most recent commit? Well, by forcing Git to jump back to an earlier commit, we have forcibly dragged `HEAD` away from the most recent commit (which was _**SHA-11b38767**_, the commit tagged `secondNEJM`) to the earlier commit of `firstNEJM`. The practical significance of this warning is that you should not change files in your directory at this point, but you may open and view them. 

Now, look at your directory.

![15_checkingout_firstNEJM](/images/15_checkingout_firstNEJM.png)

Notice the _scatter1.png_ and _scatter2.png_ have popped right back! If you open the _results.docx_, you will see the older version from the first submission. This version has the correlation analysis results in the body of the document and has the two images embedded.

![9_manuscriptv1](/images/9_manuscriptv1.png)

And if you view the do-file, you will see the code from the first submission. 

```shell
*******************************
* Do FILE TO ANALYZE LUNG.DTA *
*******************************

use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* however, the variances of the fev1s are not the same for fathers and mothers.
* variance for men is 4189.2
su ffev1, det
* variance for women is 2402.3
su mfev1, det

* let's do a t-test, but this time, REMEMBER TO USE THE UNEQUAL OPTION
ttest ffev1 == mfev1, unpaired unequal
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers

* Is there a correlation between the father's height/mother's height and the 
* oldest child's height?
pwcorr fheight mheight ocheight, sig
* Correlation for father - oldest child height (0.16)
* Correlation for mother - oldest child height (0.10)

* create and save scatterplots
scatter fheight ocheight, title("Father heights and oldest child heights") subtitle("Correlation is 0.16")
graph export scatter1.png
scatter mheight ocheight, title("Mother heights and oldest child heights") subtitle("Correlation is 0.10")
graph export scatter2.png
```

This has the correlation analysis and the lines that create and save figures included in the body of the do-file.

> **_As mentioned earlier, it is VERY IMPORTANT to not change the files at this stage. Open them up and view them to refresh your memory as to what was actually submitted, but DO NOT change anything. If you want to change stuff at this point, you will need to create something called a branch, which we will not focus on in this write-up_**.

Once you're done looking around and want to go back to the way things were, just type this in.

```shell
$ git checkout master
```

Look at your directory again and confirm that everything is back to the way it was at the point of resubmission (i.e, the scatterplot images are gone, the results doc doesn't mention the correlation analysis but instead talks about a regression analysis, and the do-file has the regression analysis code instead of the correlation analysis and graph code).

# Conclusion

That wraps up this little walkthrough about **Documentation with Git**. The things we touched upon were:

* Making a directory a Git repository, and confiming this visually using the 'View hidden files' option.
* Adding/staging files.
* Committing files and the importance of commit messages.
* Amending commit messages.
* Renaming/moving and deleting files.
* Tagging important commits.
* Checking out older versions of your project; aka time traveling with Git.

I hope that this helps you see that Git is tremendously useful to document every little step of your project[^3]. 



# References

[^1] I love Rick Umali's [Learn Git In A Month of Lunches](https://www.amazon.com/Learn-Month-Lunches-Rick-Umali/dp/1617292419) for its hands-on approach, but if you want a more detailed guide, look up Chacon and Straub's [Pro Git Book](https://git-scm.com/book/en/v2), the web version of which is totally free. Also, if you want a more verbose, elegantly written book that introduces you to the whole philosophy behind Git, you'll want to check out Demaree's delightfully named book — [Git for Humans](https://www.amazon.com/Git-Humans-David-Demaree/dp/1937557383).

[^2] The lung.dta dataset comes from [UCLA's Institute for Digital Research and Education (IDRE)](https://stats.idre.ucla.edu/other/examples/pma5/), and they in turn sourced this dataset from the 5th edition of Affi, May & Clark's Practical Multivariate Analysis(https://www.amazon.com/Practical-Multivariate-Analysis-Chapman-Statistical/dp/1439816808). This dataset has 150 records uniquely representing households, and for each household, has the age, height, weight, gender, forced expiratory volume and forced vital capacity of the lungs for each member (i.e, father, mother, oldest child, middle child, youngest child). For more details about the dataset, refer to the book.

[^3] I greatly enjoyed the Coursera course 'Version Control with Git' [offered by Steve Byrnes](https://www.coursera.org/learn/version-control-with-git), this introduced me to some of the utilities of Git.







