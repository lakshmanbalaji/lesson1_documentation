*******************************
* Do FILE TO ANALYZE LUNG.DTA *
*******************************

use lung.dta, clear

* Say we want to compare forced expiratory volume for mothers and fathers

* Checking normality
hist ffev1, title("Forced Expiratory Volumes (FEV1) for fathers") normal
hist mfev1, title("Forced Expiratory Volumes (FEV1) for mothers") normal

* variables are independent, continuous and normality distributed.
* however, the variances of the fev1s are not the same for fathers and mothers.
* variance for men is 4189.2
su ffev1, det
* variance for women is 2402.3
su mfev1, det

* let's do a t-test, but this time, REMEMBER TO USE THE UNEQUAL OPTION
ttest ffev1 == mfev1, unpaired unequal
* Mean FEV1 for men is 410.4 ± 64.7, mean for women is 297.2 ± 49.0
* There is a statistically significant difference in means of FEV1 between 
* mothers and fathers

* Per reviewer request,
* Removed correlation analysis, 
* and now we 
* regress ocheight on ocage, fheight, mheight.
regress ocheight ocage fheight mheight
* Coefficient for ocage is 1.98
* Fheight is 0.36
* Mheight is 0.28
